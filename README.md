# Instructions on how to use these snippets

1. Open Package Control inside of Sublime Text using shift + cmd + P
2. Search for: Package Control: Add Repository
3. Add the link to your repo: https://bitbucket.org/samuel_huwiler/sublime-text-snippets/
4. Now install the snippets: shift + cmd + P To launch Package Control again.
5. Start typing: Package Control: Install Package
6. Search for the repo name.
7. Start using your new snippet collection!

Once you are in javascript file you can then type rN... and select one of the templates hitting enter or tab to expand the snippet.